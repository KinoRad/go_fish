public class Card{
	private Suit suit;
	private int value;
	
	public Card(Suit suit, int value){
		this.suit = suit;
		this.value = value;
	}
	
	public Suit getSuit(){
		return suit;
	}
	
	public int getValue(){
		return value;
	}
	
	public double getValueDouble(){
		int val = this.value;
		if(suit == Suit.Hearts){
			val += 0.1;
		} else if(suit == Suit.Spades){
			val += 0.2;
		} else if(suit == Suit.Clubs){
			val += 0.3;
		} else{
			val += 0.4;
		}
		return val;
	}
	
	public String toString(){
		String tempValue = "";
		if(value == 1){
			tempValue = "Ace";
		}else if(value == 11){
			tempValue = "Jack";
		}else if(value == 12){
			tempValue = "Queen";
		}else if(value == 13){
			tempValue = "King";
		} else {
			tempValue += value;
		}
		return tempValue + " of " + suit;
	}
}