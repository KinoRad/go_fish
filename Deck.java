import java.util.Random;
public class Deck{
	private int pointer;
	private Card[] stack;
	Random rng;
	
	public Deck(){
		rng = new Random();
		
		Suit[] suits = new Suit[] {Suit.Hearts, Suit.Spades, Suit.Clubs, Suit.Dimonds};
		int[] values = new int[] {1,2,3,4,5,6,7,8,9,10,11,12,13}; 
		this.stack = new Card[100];
		this.pointer = 0;
		for(int value : values){
			for(Suit suit : suits){
				stack[pointer] = new Card(suit, value);
				this.pointer++;
			}
		}
	}
	
	public int getLength(){
		return pointer;
	}
	
	public void shuffle(){
		for(int i = 0; i < pointer; i++){
			int ranNum = rng.nextInt(pointer);
			Card placholder = stack[i];
			stack[i] = stack[ranNum];
			stack[ranNum] = placholder;
		}
	}
	
	public String toString(){
		String outPut = "";
		for(int i = 0; i < pointer; i++){
			outPut = outPut + stack[i] +" \n";
		}
		return outPut;
	}
	
	public Card drawTopCard(){
		pointer--;
		Card tempCard = stack[pointer];
		stack[pointer] = null;
		return tempCard;
	}
	public Card[] nCards(int n){
		Card[] cards = new Card[n];
		for(int i = 0; i < n; i++){
			cards[i] = drawTopCard();
		}
		return cards;
	}
}