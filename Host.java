import java.util.Scanner;
import java.util.InputMismatchException;
import java.util.Random;
public class Host{
	
	private Random random;
	private Deck d1;
	private Player[] players;
	private int playersPointer;
	private int numOfPlayers;
	private int numOfHumans;
	private int numOfBots;
	private String[] namesOfBots;
	private int namesOfBotsPointer;
	private String[] names;
	private int namesPointer;
	private int numOfCardsPerHand;
	private java.util.Scanner reader;
	private String lastAction;
	
	private static final int minNumOfCards = 4;
	private static final int maxNumOfCards = 6;
	private static final int minNumPlayers = 3;
	private static final int maxNumPlayers = 6;
	// len of deck enough for us to draw 5 cards
	private static final int lenOfDeckForFive = 5; 
	private static final int minNumOfCardsToDraw = 1;
	private static final int maxNumOfCardsToDraw = 5;
	private static final int outOfRangeNum = -1;
	private static final int minLenOfName = 1;
	private static final int maxLenOfName = 15;
	private static final int numToShowHand = 999;
	private static final boolean bot = true;
	private static final int user = 1;
	private static final int goFishCardQy = 1;
	private static final int ace = 1;
	private static final int jack = 11;
	private static final int queen = 12;
	private static final int king = 13;
	
	public Host(){
		this.reader = new java.util.Scanner(System.in);
		explainGame();
		this.random = new Random();
		this.namesOfBots = new String[] {"bot.Eric","bot.Swetha","bot.Patricia","bot.Jaya","bot.Dirk", "bot.Myles"};
		this.namesOfBotsPointer = 0;
		this.numOfPlayers = askUserForHowManyPlayers();
		this.numOfBots = askUserForBotPlayers();
		this.numOfHumans = numOfPlayers - numOfBots;
		this.lastAction = "";
		this.d1 = new Deck();
		d1.shuffle();
		this.numOfCardsPerHand = askUserHowManyCards();
		this.names = new String[this.numOfPlayers];
		this.namesPointer = 0;
		this.players = new Player[numOfPlayers];
		this.playersPointer = 0;
		createHumanplayers();
		createBots();
		updateAllPoints();
		runGame();
	}
	public int askUserForHowManyPlayers(){
		boolean condition = true;
		int playerNum = 0;
		while(condition){
			System.out.println("How many players do you want to play with?");
			System.out.println("(" + minNumPlayers + " min, " + maxNumPlayers +" max)");
			playerNum = getNumber();
			if(playerNum >= minNumPlayers && playerNum <= maxNumPlayers){
				condition = false;
			}else{
				System.out.println("Your imput does not meet the criteria! Please try again.\n");
			}
		}
		return playerNum + user; 
	}
	public int askUserForBotPlayers(){
		boolean condition = true;
		int tempNumOfBots = 0;
		while(condition){
			System.out.println("How many of those players will be bots?");
			tempNumOfBots = getNumber();
			if(tempNumOfBots > outOfRangeNum && tempNumOfBots <= numOfPlayers - user){
				condition = false;
			}else {
				System.out.println("The num of bots is out of range!");
				System.out.println("It should be less than the number of players you are playing with \n");
			}
		}
		return tempNumOfBots;
	}
	public int askUserHowManyCards(){
		boolean condition = true;
		int cardsPerHand = 0;
		while(condition){
			System.out.println("how many Cards per hand? (min " + minNumOfCards + ", max " + maxNumOfCards + ")");
			cardsPerHand = getNumber();
			if(cardsPerHand >= minNumOfCards && cardsPerHand <= maxNumOfCards){ // same
				condition = false;
			}else{
				System.out.println("Your atempts are pathetic, but keep trying... loser \n");
				System.out.println("Try again. Make it in the required range this time.");
			}
		}
		return cardsPerHand;
	}
	public boolean checkIfNameExists(String name){
		for(int i = 0; i < namesPointer; i++){
			if(names[i].equals(name)){
				return true;
			}
		}
		return false;
	}
	public void createHumanplayers() {
		for (int i = 0; i < numOfHumans; i++) {
			String name = "";
			boolean condition = true;
			while(condition){
				System.out.println("Enter a name for one of the human players (min 1 char, max 15 char)");
				name = reader.nextLine();
				if(!checkIfNameExists(name) && name.length() >= minLenOfName && name.length() <= maxLenOfName){
					condition = false;
				}else{
					System.out.println("the name you have chosen is either too short, long, or already taken \n");
				}
			}
			names[namesPointer] = name;
			namesPointer++;
			Card[] hand = d1.nCards(numOfCardsPerHand);
			players[i] = new Player(name, hand, !bot);
			playersPointer++;
			players[i].sortHand();
		}
	}
	public void createBots(){
		for(int i = playersPointer; i < numOfPlayers; i++){
			Card[] hand = d1.nCards(numOfCardsPerHand);
			String name = namesOfBots[namesOfBotsPointer];
			namesOfBotsPointer++;
			players[i] = new Player(name, hand, bot);
			playersPointer++;
		}
	}
	public Player getPlayerByName(String name){
		for(Player player : players){
			if(player.getName().equals(name)){
				return player;
			}
		}
		return players[0];
	}
	public void transferCard(int card, Player giver, Player reciver){
		Card[] tempHand = giver.giveCardAway(card);
		reciver.reciveCard(tempHand);
		reciver.sortHand();
		giver.sortHand();
	}
	public int findIndexOfPlayer(String name){
		for(int i = 0; i < players.length; i++){
			if(players[i].getName().equals(name)){
				return i;
			}
		}
		return outOfRangeNum;
	}
	public boolean doesPlayerHaveCard(Player player, int card){
		if(player.numOfNCardInHand(card) > 0){
			return true;
		} else{
			return false;
		}
	}
	public void playersTurn(Player player){
		if(!player.getIsBot()){
			clearScrean();
			showHandCheck(player);
			System.out.println(lastAction);
		}
		lastAction = "";
		lastAction += "last action: " + player.getName();
		askPlayerForCardAndPerson(player);
	}
	public void showHandCheck(Player player){
		boolean condition = true;
		int answer = 0;
		System.out.println(player.getName() + " will be playing");
		System.out.println(player.getName() + ": Enter '" + numToShowHand + "' when you want to see your hand");
		while(condition){
			answer = getNumber();
			if(answer == numToShowHand){
				condition = false;
				clearScrean();
				showHand(player);
			}else{
				System.out.println("You're funny, try again: \n");
			}
		}
	}
	public void showHand(Player player){
		System.out.println(player.getName() + ", this is your hand: ");
		System.out.println(player.getHandAsString());
	}
	public void askPlayerForCardAndPerson(Player player){
		if(player.getIsBot()){
			Player giver = getRandomePlayer(player);
			int card = player.getHighestPriority();
			processRequest(player, giver, card);
		}else{
			Player giver = askPlayerForGiver(player);
			int card = askPlayerForCard(player);
			processRequest(player, giver, card);
		}
	}
	public void processRequest(Player player, Player giver, int card){
		lastAction += " asks " + giver.getName() + " for a " + getNameOfCard(card);
		if(doesPlayerHaveCard(giver, card)){
			transferCard(card, giver, player);
			lastAction += ", and " + giver.getName() + " has it and gives it to " + player.getName(); 
		} else {
			System.out.println(giver.getName() + " does not have that card!");
			System.out.println("sorry... maybe try thinking next time");
			lastAction += ", but " + giver.getName() + " does not have it. \n" ;
			goFish(player);
		}
	}
	public String getNameOfCard(int card){
		String output = "";
		if(card == ace){
			output += "Ace";
		}else if(card == jack){
			output += "Jack";
		}else if(card == queen){
			output += "Queen";
		}else if(card == king){
			output += "King";
		}else{
			output += card;
		}
		return output;
	}
	public Player askPlayerForGiver(Player player){
		String potentialGiver = "";
		boolean conditionForName = true;
		showPlayerInGame();
		while(conditionForName){
			System.out.println("who do you want to ask?");
			potentialGiver = reader.nextLine();
			if(playerExists(potentialGiver) && isInGame(potentialGiver) && !(potentialGiver.equals(player.getName()))){
				conditionForName = false;
			}else {
				System.out.println("This person is either not playing anymore,");
			System.out.println("doesn't exsists, or is the very person asking. \nTry again \n");
			}
		}
		return getPlayerByName(potentialGiver);
	}
	public int askPlayerForCard(Player player){
		int card = outOfRangeNum;
		boolean condition = true;
		while(condition){
			System.out.println("what card do you want to ask for? (the number in the [] are what you type in)");
			card = getNumber();
			if(doesPlayerHaveCard(player, card)){
				condition = false;
			}else{
				System.out.println("You can't ask for this card since you don't have it!\n");
			}
		}
		return card;
	}
	public boolean isInGame(String name){
		return players[findIndexOfPlayer(name)].getInGame();
	}
	public boolean playerExists(String name){
		for(Player player : players){
			if(name.equals(player.getName())){
				return true;
			}
		}
		return false;
	}
	public void runGame(){
		while(areTherePlayersLeft()){
			for(Player player : players){
				updateAllPoints();
				updateDoneStatus();
				clearScrean();
				if(player.getInGame()){
					playersTurn(player);
				}
			}
		}
		System.out.println("THE GAME HAS CONCLUDED!!!!!");
		printWinner();
	}
	public Player getRandomePlayer(Player player){
		boolean condition = true;
		int playerIndex = 0;
		while(condition){
			System.out.println("numOfPlayers is " + numOfPlayers);
			playerIndex = random.nextInt(numOfPlayers);
			System.out.println("random is " + playerIndex);
			if(players[playerIndex] != player && players[playerIndex].getInGame()){
				condition = false;
			}
		}
		return players[playerIndex];
	}
	public void updateAllPoints(){
		for(Player player : players){
			player.updatePoints();
		}
	}
	public void canPlayerStillPlay(Player player){
		if(player.getNumOfCards() == 0){
			if(d1.getLength() == 0){
				if(player.getInGame()){
					player.changeInGame();
				}
			}else {
				if(d1.getLength() > lenOfDeckForFive){
					Card[] temp = d1.nCards(maxNumOfCardsToDraw);
					player.reciveCard(temp);
				} else{
					Card[] temp = d1.nCards(minNumOfCardsToDraw);
					player.reciveCard(temp);
				}
			}
		}
	}
	public void updateDoneStatus(){
		for(Player player : players){
			canPlayerStillPlay(player);
		}
	}
	public boolean isEveryoneDone(){
		for(Player player : players){
			if(player.getInGame()){
				return false;
			}
		}
		return true;
	}
	public void showPlayerInGame(){
		System.out.println("Players: ");
		for(Player player : players){
			if(player.getInGame()){
				System.out.println(player.getName()+ " ["+ player.getPoints()+" points]");
			}
		}
	}
	public boolean areTherePlayersLeft(){
		for(Player player : players){
			if(player.getInGame()){
				return true;
			}
		}
		return false;
	}
	public void goFish(Player player){
		if(d1.getLength() > 0){
			player.reciveCard(d1.nCards(goFishCardQy));
			player.sortHand();
			lastAction += "So, " + player.getName() + " draws the top card from the deck. ";
		}else{
			lastAction += " But since the deck is empty, " + player.getName() + " does not get a card.";
		}
	}	
	public void clearScrean(){
		System.out.println("\033[H\033[2J");
		System.out.flush();
	}
	public int findMostPoints(){
		int max = 0;
		for(Player player : players){
			if(player.getPoints() > max){
				max = player.getPoints();
			}
		}
		return max;
	}
	public void printWinner(){
		String winner = "";
		int points = findMostPoints();
		int counter = 0;
		for(Player player : players){
			if(player.getPoints() == points){
				if(counter == 0){
					winner += player.getName();
					counter++;
				}else{
					winner += ", " + player.getName();
				}
			}
		}
		System.out.println("WINNER(S): ");
		System.out.println(winner);
		System.out.println("with " + points + " points!!!!");
	}
	public int getNumber(){
		boolean condition = true;
		int output = 0;
		while(condition){
			try{
			String num = reader.nextLine().trim();
			output = Integer.parseInt(num); // Trim any leading or trailing whitespace
			condition = false;
			}catch(NumberFormatException e){
				System.out.println("Please try agian, and enter a number this time!\n");
			}
		}
		return output;
	}
	public void explainGame(){
		System.out.println("In Go Fish, players aim to collect sets of four cards of the same rank from a ");
		System.out.println("standard 52-card deck. At the outset, each player is dealt a hand of cards. During ");
		System.out.println("their turn, a player asks another for a specific rank they already possess in their ");
		System.out.println("hand, and if the opponent has it, they must surrender all cards of that rank. If the ");
		System.out.println("opponent doesn't have the requested card, they reply 'Go Fish,' and the asking player ");
		System.out.println("draws a card from the deck. If the drawn card completes a set, the player usually gets ");
		System.out.println("another turn. The game proceeds until all sets are completed. If there are no more cards"); 
		System.out.println("in the deck and no more matches to be made, the player with the most sets wins.\n");
		System.out.println("Type a number if you understad!");
		getNumber();
	}
}