import java.util.Random;
public class Player{
		
	private String name;
	private boolean isBot;
	private Card[] hand;
	private int handPointer;
	private int points;
	private boolean inGame;
	private Random random;
	
	private int[][] priority;
	private int maxArrayPointer;
	private int midArrayPointer;
	private int minArrayPointer;
	
	private static final int maxArraySize = 100;
	private static final int maxCardsBeforePoint = 3;
	private static final int maxPriorityReq = 3;
	private static final int midPriorityReq = 2;
	private static final int minPriorityReq = 1;
	private static final int maxPriorityIndex = 2;
	private static final int midPriorityIndex = 1;
	private static final int minPriorityIndex = 0;

	

	public Player(String name, Card[] hand, boolean isBot){
		this.random = new Random();
		this.isBot = isBot;
		this.name = name;
		this.points = 0;
		this.hand = new Card[maxArraySize];
		this.handPointer = 0;
		this.hand = new Card[maxArraySize];
		this.inGame = true;
		this.priority = new int[maxCardsBeforePoint][maxArraySize];
		this.maxArrayPointer = 0;
		this.midArrayPointer = 0;
		this.minArrayPointer = 0;
		
		reciveCard(hand);
		updatePriority(); // to do
	}
	
	public int getPoints(){
		return points;
	}
	
	public boolean getIsBot(){
		return isBot;
	}
	
	public boolean getInGame(){
		return inGame;
	}
	
	public void changeInGame(){
		inGame = !inGame;
	}
	
	public String getName(){
		return name;
	}
	
	public int getNumOfCards(){
		return handPointer;
	}
	
	public String getHandAsString(){
		String output = "";
		for(int i = 0; i < handPointer; i++){
			output += "[" + hand[i].getValue() + "] " + hand[i].toString() + "\n";
		}
		return output;
	}
	public String toString(){
		String output = "";
		for(int i = 0; i < handPointer; i++){
			output = output + hand[i] + "\n";
		}
		return output;
	}
	public boolean isCardInHand(int card){
		if(numOfNCardInHand(card) > 0){
			return true;
		}
		return false;
	}
	public int numOfNCardInHand(int card){
		int counter = 0;
		for(int i = 0; i < handPointer; i++){
			if(hand[i].getValue() == card){
				counter++;
			}
		}
		return counter;
	}
	public int numOfNCardInHandFromIndex(int card, int index){
		int counter = 0;
		for(int i = index; i < handPointer; i++){
			if(hand[i].getValue() == card){
				counter++;
			}
		}
		return counter;
	}
	public int findSmallest(int index){
		int indexOfSmallest = index;
		for(int i = index; i < handPointer; i++){
			if(hand[indexOfSmallest].getValueDouble() > hand[i].getValueDouble()){
				indexOfSmallest = i;
			}
		}
		return indexOfSmallest;
	}
	public void reciveCard(Card[] cards){
		for(Card card : cards){
			hand[handPointer] = card;
			handPointer++;
		}
	}
	public Card[] giveCardAway(int card){
		int sizeOfReturnArray = numOfNCardInHand(card);
		Card[] returnArray = new Card[sizeOfReturnArray];
		int returnArrayIndex = 0;
		
		for(int i = 0; i < handPointer; i++){
			if(hand[i].getValue() == card){
				returnArray[returnArrayIndex] = hand[i];
				returnArrayIndex++;
			}
		}
		removeFromHand(card);
		return returnArray;
	}
	public void removeFromHand(int value){
		for(int i = 0; i < handPointer; i++){
			if(hand[i].getValue() == value){
				removeCardAtIndex(i);
				i--;
			}
		}
	}
	public void removeCardAtIndex(int index){
		hand[index] = null;
		for(int i = index; i < handPointer; i++){
			hand[i] = hand[i + 1];
		}
		handPointer--;
	}
	public void swapnumOfNCardInHand(int c1, int c2){
		Card placeHolder;
		placeHolder = hand[c1];
		hand[c1] = hand[c2];
		hand[c2] = placeHolder;
	}
	public void sortHand(){
		for(int i = 0; i < handPointer; i++){
			int smallestCard = findSmallest(i);
			swapnumOfNCardInHand(i, smallestCard);
		}
	}
	public void updatePoints(){
		for(int i = 0; i < handPointer; i++){
			if(numOfNCardInHandFromIndex(hand[i].getValue(), i) == 4){
				removeFromHand(hand[i].getValue());
				i--;
				points++;
			}
		}
	}
	public void clearPriority(){
		this.priority = new int[maxCardsBeforePoint][maxArraySize];
		this.maxArrayPointer = 0;
		this.midArrayPointer = 0;
		this.minArrayPointer = 0;
	}
	public void updatePriority(){
		clearPriority();
		int[] uniqueCardsInHand = uniqueCards();
		for(int card : uniqueCardsInHand){
			if(numOfNCardInHand(card) == maxPriorityReq){
				priority[maxPriorityIndex][maxArrayPointer] = card;
				maxArrayPointer++;
			}else if(numOfNCardInHand(card) == midPriorityReq){
				priority[midPriorityIndex][midArrayPointer] = card;
				midArrayPointer++;
			} else{
				priority[minPriorityIndex][minArrayPointer] = card;
				minArrayPointer++;
			}
		}
	}
	public int[] uniqueCards(){
		int[] tempArray = new int[maxArraySize];
		int tempPointer = 0;
		for(int k = 0; k < handPointer; k++){
			boolean isCardFound = false;
			for(int i = 0; i < tempPointer; i++){
				if(tempArray[i] == hand[k].getValue()){
					isCardFound = true;
				}
			}
			if(!isCardFound){
				tempArray[tempPointer] = hand[k].getValue();
				tempPointer++;
			}
		}
		return getTraditionalArray(tempArray, tempPointer);
	}
	public int[] getTraditionalArray(int[] array, int pointer){
		int[] tradArray = new int[pointer];
		for(int i = 0; i < pointer; i++){
			tradArray[i] = array[i];
		}
		return tradArray;
	}
	public int getHighestPriority(){
		updatePriority();
		if(maxArrayPointer > 0){
			return priority[maxPriorityIndex][random.nextInt(maxArrayPointer)];
		}else if(midArrayPointer > 0){
			return priority[midPriorityIndex][random.nextInt(midArrayPointer)];
		} else {
			return priority[minPriorityIndex][random.nextInt(minArrayPointer)];
		}
	}
}