April 23 2024
------------
- this session goals:
	- implement reciveCard(), [x]
		giveCardAway(), [x]
		removeFromHand(), [x]
		removeCardAtIndex() [x]
	- test in the Testing.java [x]
- last session achievment:
	- create the git repo [x]
	- fix the getValueDouble() [x]
----------------------------------------------
April 25 2024
------------
- this session goal:
	- fix the sort hand method [x]
	- test it in Testing.java [x]
	- implement the askPlayerForCard();	[not done]
- last session achievments:
	implement reciveCard(), [x]
		giveCardAway(), [x]
		removeFromHand(), [x]
		removeCardAtIndex() [x]
-----------------------------------------------
April 27 2024
-------------
- this session goal:
	- create the Player class
	- implement the askPlayerForCard() methode [not done]
- last session achievments:
	- - fix the sort hand method [x]
	- test it in Testing.java [x]
-----------------------------------------------
April 30 2024
-------------
- this session goal:
	- implement the askPlayerForCard() methode []
	- implement the transerferCard feature [x]
- last session achievments:
	- created Player class
------------------------------------------------
May 01 2024
-----------
- this session goal:
	- implement the updatePoints() method [x]
	- change the printHand() method [x]
	- add a field to the player class inGame[x]
	- add a getter and setter method for this new field [x]
	- change the structure of how the game works
		basically, I made it so that it will first ask the user if
		they want to play with bots or people, and then work from ther. [x]
	- start on the structure of the runHumanGame() method [x]
	- implement canPlayerStillPlay(); [x]
-last session achievments:
	- implement the transerferCard feature [x]
--------------------------------------------------
May 02 2024
-----------
- this session goals:
	- CHANGE THE READER TO BE BETTER []
	- implement the printEverything() method [x]
- last session achievments:
	- implement the updatePoints() method [x]
	- change the printHand() method [x]
	- add a field to the player class inGame[x]
	- add a getter and setter method for this new field [x]
	- change the structure of how the game works
		basically, I made it so that it will first ask the user if
		they want to play with bots or people, and then work from ther. [x]
	- start on the structure of the runHumanGame() method [x]
	- implement canPlayerStillPlay(); [x]
-----------------------------------------------------
May 03 2024
-----------
- this session goals:
	- fix the damn points bug already [x]
	- CHANGE THE READER TO BE BETTER [x]
	- implement the printEverything() method [x]
	- make the player turn work [] 
		- make it able to find when there are no players left in the game [] 
		- test if the game works [] 
- last session achievmenta:
	- - implement the printEverything() method [x]
------------------------------------------------------
May 08 2024
-----------
- this session goals:
	- make the base game playable [x]
		- make the method that checks if there are any players left [x]
		- make method that checks if players need to have cards added to their hand [x]
		- make all the helper methods needed for the previous 2 methods [x]
		- put it all methods together [x]
		- remove the trash code [x] 
		- make a method that clears the terminal [x]
	- make sure that the code can't be broken by user imput [x]
		- add try/catch everywhere where there is user imput [x]
		- find somewhere to throw an exeption [x]
- last session achievments:
	- fix the damn points bug already [x]
	- CHANGE THE READER TO BE BETTER [x]
	- implement the printEverything() method [x]
		
	---> THE BASE GAME IS NOW OFFICIALLY PLAYABLE!!! (patting myself on the back)
-------------------------------------------------------
May 09 2024
-----------
- this session goals:
	- make the game incorporate bots [x]
		- build the helper functions for the bots [x]
		- add the required atrubutes [x]
- last session achievments:
	- make the base game playable [x]
		- make the method that checks if there are any players left [x]
		- make method that checks if players need to have cards added to their hand [x]
		- make all the helper methods needed for the previous 2 methods [x]
		- put it all methods together [x]
		- remove the trash code [x] 
		- make a method that clears the terminal [x]
	- make sure that the code can't be broken by user imput [x]
		- add try/catch everywhere where there is user imput [x]
		- find somewhere to throw an exeption [x]
---------------------------------------------------------
May 13 2024
-----------
- this session goals:
	- make the Player.java class support both bots and humans: [x]
		- create the helper functions for bots in Player.java: [x]
			- getHighestPriority(), [x]
			- getTraditionalArray(), [x]
			- uniqueCards(), [x]
			- updatePriority(), [x] 
			- clearPriority(), [x]
			- updatePoints() [x]
		- create the attributs for the bots [x]
			- boolean isBot; [x]
	- make Host.java account for if the user whats to play with bots, people, or a mix [x]
		- refactor the playerTurn() method and askUserForInput() [x]
		- create the processRequest() method [x]
	- remove usless and duplicate code [x]
		- create the getNumber() method [x]
		- remove all try/catch that isn't in the getNumber() method [x]
	- test the game [x]
	- delete the usless .java class files [x]
		- delete AiPlayer.java (it can be replaced by Player.java) [x]
		- delete Bot.java (it was supposed to be a newer version of AiPlayer.java) [x]
		- delete Testing.java (it was just where I tested things out) [x]
	- create GoFish.java (I will call the game from there) [x]
	- add the game explanation [x]
- last session achievments:
	- make the game incorporate bots [x]
		- build the helper functions for the bots [x]
		- add the required atrubutes [x]
--------->>>>>> THE PROJECT IS COMPLETE!!!!!!!